package slack

import (
	"bytes"
	"encoding/json"
	"fmt"

	"git.sussexdirectories.com/sussex/phi-reporter/internal/httpcli"
)

// SendMessage takes message and url as string arguments and calls slack webhook with valid payload
func SendMessage(msg, url string) (err error) {
	var slackPld = struct {
		Text string `json:"text"`
	}{
		Text: msg,
	}

	jb, err := json.Marshal(slackPld)
	if err != nil {
		return fmt.Errorf("failed marshaling slack msg payload %w", err)
	}

	req := httpcli.
		New("POST", url, bytes.NewReader(jb)).
		AddHeader("Content-type", "application/json").
		Do()

	if err = req.HasError(); err != nil {
		return fmt.Errorf("failed posting slack message %w", err)
	}

	return
}

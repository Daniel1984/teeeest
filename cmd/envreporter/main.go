package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"git.sussexdirectories.com/sussex/phi-reporter/internal/slack"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const (
	envGitApiKey            = "GITLAB_API_KEY"
	envGitApiUrlKey         = "GITLAB_API_URL"
	envProjectIDsKey        = "PROJECT_IDS"
	envSlackWebhookKey      = "SLACK_WEBHOOK"
	reportEnvsOlderThanDays = 7
)

func main() {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})

	gitlabApiKey, ok := os.LookupEnv(envGitApiKey)
	if !ok {
		logger.Fatalf("missing %s env var", envGitApiKey)
	}

	gitlabApiURL, ok := os.LookupEnv(envGitApiUrlKey)
	if !ok {
		logger.Fatalf("missing %s env var", envGitApiUrlKey)
	}

	gitlabProjectIDs, ok := os.LookupEnv(envProjectIDsKey)
	if !ok {
		logger.Fatalf("missing %s env var", envProjectIDsKey)
	}

	if len(gitlabProjectIDs) == 0 {
		logger.Fatalf("project IDs must be sent under %s env var", envProjectIDsKey)
	}

	slackWebhook, ok := os.LookupEnv(envSlackWebhookKey)
	if !ok {
		logger.Fatalf("missing %s env var", envSlackWebhookKey)
	}

	git, err := gitlab.NewClient(gitlabApiKey, gitlab.WithBaseURL(gitlabApiURL))
	if err != nil {
		logger.Fatalf("failed to create client: %s", err)
	}

	// filter specification, you can add more if needed as ling as they satisfy EnvFilterSpec type
	envFilter := EnvFilter{
		Specifications: []EnvFilterSpec{
			excludeDevDevelopEnv,
			excludeQaEnv,
			excludeProdEnv,
			excludeNewEnvs,
		},
	}

	msg := strings.Builder{}
	msg.WriteString("*Stale Environments Report*\n\n")

	for _, pID := range strings.Split(gitlabProjectIDs, ",") {
		projectID, err := strconv.Atoi(pID)
		if err != nil {
			logger.Errorf("converting project id from string: %d to int, err: %s", projectID, err)
			continue
		}

		proj, _, err := git.Projects.GetProject(projectID, nil)
		if err != nil {
			logger.Errorf("failed to get project: %d, err: %s", projectID, err)
			continue
		}

		msg.WriteString(fmt.Sprintf("*Project:* %s\n", proj.NameWithNamespace))
		msg.WriteString(fmt.Sprintf("*Project URL:* %s\n\n", proj.Namespace.WebURL))

		envs, _, err := git.Environments.ListEnvironments(projectID, &gitlab.ListEnvironmentsOptions{
			ListOptions: gitlab.ListOptions{Page: 0, PerPage: 100},
			States:      gitlab.String("available"),
		})
		if err != nil {
			logger.Errorf("failed fetching environments for project: %d, err: %s", projectID, err)
			continue
		}

		envs = envFilter.Filter(envs)

		if len(envs) == 0 {
			msg.WriteString("no stale environments found\n")
			msg.WriteString("-----------------------------------------------------\n\n")
			continue
		}

		for i, env := range envs {
			msg.WriteString(fmt.Sprintf("%d. *id:* %d\n", i+1, env.ID))
			msg.WriteString(fmt.Sprintf("\t*name:* %s\n", env.Name))
			msg.WriteString(fmt.Sprintf("\t*state:* %s\n", env.State))
			msg.WriteString(fmt.Sprintf("\t*created:* %s\n", env.CreatedAt))
		}
		msg.WriteString("-----------------------------------------------------\n\n")
	}

	msg.WriteString("*MAKE SURE TO DELETE ALL STALE ENVIRONMENTS ALONG WITH ANY DEPLOYED AWS RESOURCES!*\n")

	fmt.Println(msg.String())

	if err := slack.SendMessage(msg.String(), slackWebhook); err != nil {
		logger.Errorf("failed sending slack message: %s", err)
		os.Exit(1)
	}

	os.Exit(0)
}

// EnvFilterSpec gitlab environment resource filter specification type
type EnvFilterSpec func(*gitlab.Environment) bool

// below specifying all the filters, more can be added if needed
func excludeDevDevelopEnv(env *gitlab.Environment) bool {
	return env.Name != "dev-develop"
}

func excludeQaEnv(env *gitlab.Environment) bool {
	return !strings.HasPrefix(env.Name, "qa")
}

func excludeProdEnv(env *gitlab.Environment) bool {
	return !strings.HasPrefix(env.Name, "prod")
}

func excludeNewEnvs(env *gitlab.Environment) bool {
	daysAgo := time.Now().AddDate(0, 0, reportEnvsOlderThanDays*-1)
	return env.CreatedAt.Before(daysAgo)
}

// EnvFilterSpec gitlab environment resource filter used as OCP + Specification pattern
type EnvFilter struct {
	Specifications []EnvFilterSpec
}

// Filter filters out unvanted gitlab environments dependong on specification
func (ef EnvFilter) Filter(envs []*gitlab.Environment) []*gitlab.Environment {
	filteredEnvs := make([]*gitlab.Environment, 0)
	for _, env := range envs {
		if ef.SpecMet(env) {
			filteredEnvs = append(filteredEnvs, env)
		}
	}
	return filteredEnvs
}

// SpecMet runs through list of filter specs and returns true only if all specs pass
func (ef EnvFilter) SpecMet(env *gitlab.Environment) bool {
	for _, specPass := range ef.Specifications {
		if !specPass(env) {
			return false
		}
	}

	return true
}

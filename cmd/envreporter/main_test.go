package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

var created = timePointer(time.Now().AddDate(-1, 0, 0))

func TestFilters(t *testing.T) {
	testCases := map[string]struct {
		Given    []*gitlab.Environment
		Expected []*gitlab.Environment
		Filters  []EnvFilterSpec
	}{
		"exclude dev envs": {
			Given: []*gitlab.Environment{
				{Name: "dev-develop"},
				{Name: "dev-foo"},
				{Name: "dev-bar"},
			},
			Expected: []*gitlab.Environment{
				{Name: "dev-foo"},
				{Name: "dev-bar"},
			},
			Filters: []EnvFilterSpec{
				excludeDevDevelopEnv,
			},
		},
		"exclude qa envs": {
			Given: []*gitlab.Environment{
				{Name: "qa01"},
				{Name: "dev-foo"},
				{Name: "dev-bar"},
			},
			Expected: []*gitlab.Environment{
				{Name: "dev-foo"},
				{Name: "dev-bar"},
			},
			Filters: []EnvFilterSpec{
				excludeQaEnv,
			},
		},
		"exclude prod envs": {
			Given: []*gitlab.Environment{
				{Name: "prod"},
				{Name: "dev-foo"},
				{Name: "dev-bar"},
			},
			Expected: []*gitlab.Environment{
				{Name: "dev-foo"},
				{Name: "dev-bar"},
			},
			Filters: []EnvFilterSpec{
				excludeProdEnv,
			},
		},
		"exclude new envs": {
			Given: []*gitlab.Environment{
				{Name: "dev-foo", CreatedAt: timePointer(time.Now())},
				{Name: "dev-bar", CreatedAt: created},
			},
			Expected: []*gitlab.Environment{
				{Name: "dev-bar", CreatedAt: created},
			},
			Filters: []EnvFilterSpec{
				excludeNewEnvs,
			},
		},
	}

	for k, v := range testCases {
		ef := EnvFilter{
			Specifications: v.Filters,
		}

		t.Run(k, func(t *testing.T) {
			got := ef.Filter(v.Given)
			assert.Equal(t, v.Expected, got)
		})
	}
}

func timePointer(t time.Time) *time.Time {
	return &t
}
